# Kafka Demo
This is a sample project that demonstrates how to produce and consume messages with Kafka.


## Project Setup

1. Clone the repository to your local machine: git clone https://gitlab.com/QusaiKuhail/accounts-loans-cards-services.git
2. Navigate to the project directory: cd KafkaDemo
3. Build and run the project: ./mvnw spring-boot:run




The project will start, and you can access the APIs.

## APIs

### Consume API
- **URL**: `http://localhost:9080/consume`
- **HTTP Method**: GET

### Request Parameters
- `topic` (required): The name of the topic
- `key` (required): The key of the needed messages

Use this API to consume messages from a Kafka topic. Provide the topic and key as query parameters.

### Produce API

- **URL**: `http://localhost:9080/produce`
- **HTTP Method**: POST

Use this API to produce a message to a Kafka topic. You need to provide a JSON request body with the message details.

Example Request:
```json
POST http://localhost:9080/produce
Content-Type: application/json

{
"topic": "demo_java",
"key": "id_3",
"value": "Your message content here"
}
