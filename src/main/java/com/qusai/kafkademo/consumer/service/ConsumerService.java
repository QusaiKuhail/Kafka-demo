package com.qusai.kafkademo.consumer.service;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.qusai.kafkademo.producer.service.ProducerService;

@Service
public class ConsumerService {
	private static final Logger log = LoggerFactory.getLogger(ProducerService.class.getSimpleName());

	private Properties getProperties() {
		// Create Consumer Properties
		Properties properties = new Properties();

		// Connect to the cluster
		try (InputStream inputStream = ProducerService.class.getClassLoader()
				.getResourceAsStream("kafka-config-consumer.properties")) {
			if (inputStream != null) {
				properties.load(inputStream);
			}
		} catch (IOException e) {
			log.error("Error while reading Kafka configurations" + e);
		}

		return properties;
	}

	public List<String> consume(String topic, String key) {
		List<String> messages = new ArrayList<>();
		boolean timeFlag = true;
		long startTime = System.currentTimeMillis();
		// Create a consumer
		KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(getProperties());

		// get a reference to the main thread
		final Thread mainThread = Thread.currentThread();
		// adding the shutdown hool
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				log.info("Detected a shutdown,let's exit by calling consumer.wakeup()...");
				consumer.wakeup();

				// join the main thread to allow the execution of the code in the main thread
				try {
					mainThread.join();
				} catch (InterruptedException e) {
					log.error("InterruptedException", e);
				}
			}
		});

		try {
			// subscribe to a topic
			consumer.subscribe(Arrays.asList(topic));
			// poll for data
			while (timeFlag) {
				log.info("Polling");
				ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
				for (ConsumerRecord<String, String> record : records) {
					log.info("Key: " + record.key() + ", Value: " + record.value());
					log.info("Partition: " + record.partition() + ", Offset: " + record.offset());
					if (record.key() != null && record.value() != null && record.key().equalsIgnoreCase(key)) {
						messages.add(record.value());
					}

				}
				if (System.currentTimeMillis() - startTime > 40000) {
					return messages;
				}
			}
		} catch (WakeupException e) {
			log.info("Consumer is starting to shutdown");
		} catch (Exception e) {
			log.error("Unexpected exception in the consumer", e);
		} finally {
			consumer.close();
			log.info("The consumer is now gracefully shut down");
		}

		return messages;

	}

	public List<String> getAllowedTopics() {
		List<String> topicsList = new ArrayList<>();
		Properties properties = new Properties();

		try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("topics.properties")) {
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String allowedTopics = properties.getProperty("allowed.topics");

		if (allowedTopics != null) {
			String[] topics = allowedTopics.split(",");
			for (String topic : topics) {
				topicsList.add(topic.trim());
			}
		} else {
			log.error("No 'allowed.topics' property found in the properties file.");
		}

		return topicsList;
	}

}
