package com.qusai.kafkademo.consumer.controller;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.qusai.kafkademo.consumer.service.ConsumerService;


@RestController
public class ConsumerController {
	@Autowired
	ConsumerService service;

	@GetMapping("/consume")
	public List<String> getRecords(@RequestParam String topic, @RequestParam String key) {
		if (!service.getAllowedTopics().contains(topic)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Topic");
		}

		List<String> records = service.consume(topic, key);

		if (records.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NO_CONTENT, "No Messages to consume");
		}

		return records;
	}
}
