package com.qusai.kafkademo.producer.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ProducerService {
	private static final Logger log = LoggerFactory.getLogger(ProducerService.class.getSimpleName());

	private Properties getProperties() {
		// Create Producer Properties
		Properties properties = new Properties();

		// Connect to the cluster
		try (InputStream inputStream = ProducerService.class.getClassLoader()
				.getResourceAsStream("kafka-config-producer.properties")) {
			if (inputStream != null) {
				properties.load(inputStream);
			}
		} catch (IOException e) {
			log.error("Error while reading Kafka configurations" + e);
		}

		return properties;
	}

	public void produce(String topicName, String key, String value) {
		KafkaProducer<String, String> producer = new KafkaProducer<String, String>(getProperties());

	
		// Create a Producer Record
		ProducerRecord<String, String> producerRecord = new ProducerRecord<>(topicName, key, value);

		
		// Send Data with a Lambda Expression for the Callback
		producer.send(producerRecord, (recordMetadata, e) -> {
			// executes every time a record is successfully sent or an exception is thrown
			if (e == null) {
				// the record is successfully sent
				log.info("Key: " + key + "\n" + "Topic: " + recordMetadata.topic() + "\n " + "Partition: "
						+ recordMetadata.partition() + "\n " + "Offset: " + recordMetadata.offset() + "\n ");
			} else {
				log.error("Error while Producing", e);
			}
		});

		// Flush the producer -- Tell the producer to send all data and block until done
		producer.flush();

		// Flush and close the producer
		producer.close();
	}

	public List<String> getAllowedTopics() {
		List<String> topicsList = new ArrayList<>();
		Properties properties = new Properties();

		try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("topics.properties")) {
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String allowedTopics = properties.getProperty("allowed.topics");

		if (allowedTopics != null) {
			String[] topics = allowedTopics.split(",");
			for (String topic : topics) {
				topicsList.add(topic.trim());
			}
		} else {
			log.error("No 'allowed.topics' property found in the properties file.");
		}

		return topicsList;
	}

}
