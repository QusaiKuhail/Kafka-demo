package com.qusai.kafkademo.producer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.qusai.kafkademo.producer.exception.FieldTypeValidationException;
import com.qusai.kafkademo.producer.model.Producer;
import com.qusai.kafkademo.producer.service.ProducerService;

import jakarta.validation.Valid;

@RestController
public class ProducerController {

	@Autowired
	ProducerService service;

	@PostMapping("/produce")
	public ResponseEntity<String> produce(@Valid @RequestBody Producer producer) {
		String topic = producer.getTopic();

		if (!service.getAllowedTopics().contains(topic)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Topic");
		}

		try {
			service.produce(topic, producer.getKey(), producer.getValue());
			return ResponseEntity.ok("Message Produced Successfully");
		} catch (FieldTypeValidationException ex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Field type error");
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error");
		}
	}

}
