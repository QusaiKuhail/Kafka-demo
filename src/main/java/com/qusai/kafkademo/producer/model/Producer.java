package com.qusai.kafkademo.producer.model;

import jakarta.validation.constraints.NotBlank;

public class Producer {

    @NotBlank(message = "Field topic is required.")
	private String topic;
    @NotBlank(message = "Field key is required.")
	private String key;
    @NotBlank(message = "Field value is required.")
	private String value;

	public Producer(String topic, String key, String value) {
		this.topic = topic;
		this.key = key;
		this.value = value;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
