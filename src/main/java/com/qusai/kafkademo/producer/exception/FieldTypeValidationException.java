package com.qusai.kafkademo.producer.exception;

public class FieldTypeValidationException extends RuntimeException {
	public FieldTypeValidationException(String message) {
        super(message);
    }

}
