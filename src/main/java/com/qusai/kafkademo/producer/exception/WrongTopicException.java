package com.qusai.kafkademo.producer.exception;

public class WrongTopicException extends RuntimeException {
	public WrongTopicException(String message) {
        super(message);
    }

}
